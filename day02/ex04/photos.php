#!/usr/bin/env php
<?php
// curl page
// regex <img src="">
// dl src

	function dl_web_imgs($url) {
		preg_match('/^(?:http[s]?:\/\/)?(?P<domain>[^\/]*)/', $url, $domain);
		$domain = $domain[domain];

		if ($domain === "")
			return;
		$dl_html = curl_init($url);

		curl_setopt_array($dl_html,
			 array(	CURLOPT_URL            => $url,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HEADER         => 0,
					CURLOPT_RETURNTRANSFER => true));
		$html = curl_exec($dl_html);
		curl_close($dl_html);

		preg_match_all('/<\s*img[^>]*?src\s*=\s*([\"\'])(?P<link>(?:\\\1|(?:(?!\1)).)*)(\1)[^>]*>/', $html, $imgs);

		if ($imgs[link] != NULL) {
			if (!file_exists($domain))
				mkdir($domain);
			$img_dl = curl_init();
			foreach ($imgs[link] as $img_url) {
				preg_match('/([^\/]*)$/', $img_url, $fname);
				$fname = "./".$domain."/".$fname[1];
				if (!file_exists($fname)) {
					$file_handle = fopen($fname, "w");
					if (strncasecmp($img_url, 'http', 4) != 0)
						$img_url = $domain."/".$img_url;
					curl_setopt_array($img_dl,
						array(
							CURLOPT_URL  => $img_url,
							CURLOPT_FOLLOWLOCATION => true,
							CURLOPT_FILE => $file_handle));
					curl_exec($img_dl);
					fclose($file_handle);
				}
			}
			curl_close($img_dl);
		}
	}

	if ($argc > 1) {
		dl_web_imgs($argv[1]);
	}

?>
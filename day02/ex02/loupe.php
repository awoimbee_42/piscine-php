#!/usr/bin/env php
<?php

	if ($argc > 1 && file_exists($argv[1])) {
		$file = file_get_contents($argv[1]);
		preg_match_all('/(?:<\s*a[^>]*?>(?:.|\n)*?<\s*\/\s*a\s*>)/', $file, $a_links, PREG_OFFSET_CAPTURE);

		foreach ($a_links[0] as $NUCLEAR_FALLOUT) {
			$NoTaVaRiAbLe = substr($file, $NUCLEAR_FALLOUT[1], strlen($NUCLEAR_FALLOUT[0]));
			$DEFENITIVELYNOTANOFFSETISWEAR = $NUCLEAR_FALLOUT[1];

			preg_match_all('/title\s*=\s*\"(?P<titles>[^"]*)\"/', $NoTaVaRiAbLe, $titlés, PREG_OFFSET_CAPTURE);
			foreach ($titlés[titles] as $title) {
				$file = substr_replace($file, strtoupper(substr($file, $title[1] + $DEFENITIVELYNOTANOFFSETISWEAR, strlen($title[0]))), $title[1] + $DEFENITIVELYNOTANOFFSETISWEAR, strlen($title[0]));
			}

			preg_match_all('/>(?P<nottitle>[^<]+)</', $NoTaVaRiAbLe, $noEsElTitleSenior, PREG_OFFSET_CAPTURE);
			foreach ($noEsElTitleSenior[nottitle] as $suicidal_thought) {
				$file = substr_replace($file, strtoupper(substr($file, $suicidal_thought[1] + $DEFENITIVELYNOTANOFFSETISWEAR, strlen($suicidal_thought[0]))), $suicidal_thought[1] + $DEFENITIVELYNOTANOFFSETISWEAR, strlen($suicidal_thought[0]));
			}
		}
		echo $file;
	}
?>
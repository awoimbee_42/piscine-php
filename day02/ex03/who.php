#!/usr/bin/env php
<?php
	// sizeof(utmpx) = 628
	// sizeof(pid_t) = 4
	date_default_timezone_set('Europe/Paris');
	$file = fopen("/var/run/utmpx", r);

	$data = fread($file, 628); // skip file header
	$data = fread($file, 628); // skip system boot
	while ($data = fread($file, 628)) {
		$array = unpack("a256ut_user/a4ut_id/a32ut_line/Lut_pid/vut_type/c2pad/lut_timevalsec/lut_timevalusec/a256ut_host",
			$data);
		$date = date('M d H:i', $array[ut_timevalsec]);
		$output = $array[ut_user]." ".$array[ut_line]."  ".$date." \n";
		$output = str_replace("\0", "", $output);
		echo $output;
	}
?>

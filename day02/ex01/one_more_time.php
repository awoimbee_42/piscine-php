#!/usr/bin/env php
<?php
	//	Python:
		// months = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"]
		// print("preg_match(\n'/", sep="", end="");
		// for m in months:
		//     print("[", m[0].capitalize(), m[0], "]", m[1:], sep="", end="|")
		// print("/'\n...", sep="");

		// i = 0
		// print("switch ($date[month]) {")
		// for m in months:
		//     print("\ncase preg_match('/[", m[0].capitalize(), m[0], "]", m[1:], "/', $date[month]) :\n\t$date[monthnb] = ", i, ";\n\tbreak;", sep="", end="")
		//     i += 1

	if ($argc > 1) {
		date_default_timezone_set('Europe/Paris');
		preg_match(
			'/^(?P<dayname>[Ll]undi|[Mm]ardi|[Mm]ercredi|[Jj]eudi|[Vv]endredi|[Ss]amedi|[Dd]imanche){1} (?P<daynb>[0-2]?[0-9]|3[01]) (?P<month>[Jj]uin|[Dd]écembre|[Ss]eptembre|[Nn]ovembre|[Jj]anvier|[Oo]ctobre|[Aa]vril|[Mm]ars|[Ff]évrier|[Jj]uillet|[Aa]oût|[Mm]ai){1} (?P<year>[0-9]{4}) (?P<time>(?:[01][0-9]|2[0-3]):(?:[0-5][0-9]):(?:[0-5][0-9]))$/',
			$argv[1],
			$date);
		if ($date[dayname] != "" && $date[daynb] != "" && $date[month] != "" && $date[year] != "" && $date[time] != "") {
			switch (TRUE) {
				case preg_match('/[Jj]anvier/', $date[month]) :
					$date[monthnb] = '01';
					break;
				case preg_match('/[Ff]évrier/', $date[month]) :
					$date[monthnb] = '02';
					break;
				case preg_match('/[Mm]ars/', $date[month]) :
					$date[monthnb] = '03';
					break;
				case preg_match('/[Aa]vril/', $date[month]) :
					$date[monthnb] = '04';
					break;
				case preg_match('/[Mm]ai/', $date[month]) :
					$date[monthnb] = '05';
					break;
				case preg_match('/[Jj]uin/', $date[month]) :
					$date[monthnb] = '06';
					break;
				case preg_match('/[Jj]uillet/', $date[month]) :
					$date[monthnb] = '07';
					break;
				case preg_match('/[Aa]oût/', $date[month]) :
					$date[monthnb] = '08';
					break;
				case preg_match('/[Ss]eptembre/', $date[month]) :
					$date[monthnb] = '09';
					break;
				case preg_match('/[Oo]ctobre/', $date[month]) :
					$date[monthnb] = '10';
					break;
				case preg_match('/[Nn]ovembre/', $date[month]) :
					$date[monthnb] = '11';
					break;
				case preg_match('/[Dd]écembre/', $date[month]) :
					$date[monthnb] = '12';
					break;
				default:
					$date[monthnb] = '00';
					break;
			}
			if (checkdate($date[monthnb], $date[daynb], $date[year])) {
				$date[daynb] = str_pad($date[daynb], 2, '0', STR_PAD_LEFT);
				$eng_date = $date[year].":".$date[monthnb].":".$date[daynb]." ".$date[time];
			}
		}
		$timestamp = strtotime($eng_date);
		if ($timestamp == "")
			echo "Wrong Format\n";
		else
			echo $timestamp."\n";
	}
?>
#!/usr/bin/env php
<?php
	// Usage:
	//		cat peer_notes_1.csv | ./agent_stats.php average
	//		cat peer_notes_1.csv | ./agent_stats.php average_user
	//		cat peer_notes_1.csv | ./agent_stats.php moulinette_variance

	// if ($argv[1] === "average" || $argv[1] === "average_user" || $argv[1] === "moulinette_variance") {
	$dict = ["name" => 1, "surname" => 2, "mail" => 3, "IP" => 4, "pseudo" => 5];

	if ($argc > 2 && file_exists($argv[1]) && ($csv = fopen($argv[1], "r")) && $dict[$argv[2]] != NULL) {
		$stdin = fopen("php://stdin", "r");
		// $csv = fopen($argv[1], "r");

		fgetcsv ($csv, 100, ";" ); // remove first line
		$name = [];
		$last_name = &$name;
		$surname = [];
		$mail = [];
		$IP = [];
		$pseudo = [];


		$key = $dict[$argv[2]] - 1;


		while ($data = fgetcsv ($csv, 100, ";" )) {
			$name[$data[$key]] = $data[0];
			$surname[$data[$key]] = $data[1];
			$mail[$data[$key]] = $data[2];
			$IP[$data[$key]] = $data[3];
			$pseudo[$data[$key]] = $data[4];
		}
		fclose($csv);

		while (true) {
			echo "Enter your command: ";
			$raw_input = fgets($stdin);
			if ($raw_input == FALSE)
				break;
			$line = trim($raw_input);
			eval($line);
		}
		echo "\n";
		fclose($stdin);
	}
?>

#!/usr/bin/env php
<?php
	if ($argc > 1 && file_exists($argv[1]) && ($srt = fopen($argv[1], "r"))) {

		$times;
		$i = 0;
		while (1) {
			$id = fgets($srt);
			if ($id == NULL || $id == "")
				break;
			$time_chelou = trim(fgets($srt));
			preg_match('/(?P<time1>[0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]{3}) --> (?P<time2>[0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]{3})/', $time_chelou, $tim);
			$times[$i][time1] = $tim[time1];
			$times[$i][time2] = $tim[time2];
			$times[$i][time_int] = floatval(str_replace(":", "", $tim[time1]));
			$times[$i][text] = trim(fgets($srt));
			$empty_line = fgets($srt);
			$i++;
		}
		usort($times, function($a,$b){return $a['time_int']-$b['time_int'];});
		// print_r($times);
		$j = 0;
		while ($j < $i) {
			echo $j + 1, "\n", $times[$j][time1], " --> ", $times[$j][time2], "\n", $times[$j][text], "\n";
			$j++;
			if ($j < $i)
				echo "\n";
		}
		fclose($srt);
	}
?>

<?php
	function auth($login, $passwd) {
		$passwd_file = "../private/passwd";
		if (file_exists($passwd_file))
			$content = unserialize(file_get_contents($passwd_file));
		$user_exists = $content[$login] != NULL;

		if ($content[$login] != NULL && hash("whirlpool", $passwd) === $content[$login]['passwd'])
			return (TRUE);
		else
			return (FALSE);
	}
?>
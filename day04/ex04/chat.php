<?php
	session_start();

	date_default_timezone_set('Europe/Paris');
	if (file_exists('../private/chat')) {
		$tmp = fopen("../private/chat", 'c+');
		@flock($tmp, LOCK_SH | LOCK_EX);
		$chat_content = unserialize(file_get_contents('../private/chat'));
		@flock($tmp, LOCK_UN);
		fclose($tmp);
		foreach($chat_content as $chat_entry) {
			$login = @$chat_entry['login'];
			$time = date('H:i', $chat_entry['time']);
			$msg = @$chat_entry['msg'];

			echo "<div class='msg'>\n";
			echo "	<i style='color: grey'>$login posted at $time :</i>\n";
			echo "	<p style='margin: 0 0 0 0'>", $msg, "</p><br />\n";
			echo "</div>";
		}
	}


?>
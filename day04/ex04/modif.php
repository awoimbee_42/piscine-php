<?php
	session_start();
	$passwd_file = "../private/passwd";
	if (file_exists($passwd_file)) {
		$content = unserialize(file_get_contents($passwd_file));
		$user_exists = $content[$_POST['login']] != NULL;

		if ($_POST['submit'] === 'OK' && $_POST['newpw'] != NULL && $user_exists && hash("whirlpool", $_POST['oldpw']) === $content[$_POST['login']]['passwd']) {
			$content[$_POST['login']] = array('login' => $_POST['login'], 'passwd' => hash("whirlpool", $_POST['newpw']));
			file_put_contents($passwd_file, serialize($content), LOCK_EX);
			$_SESSION['loggued_on_user'] = $_POST['login'];
			echo "OK\n";
			header('Location: index.html');
		}
		else {
			echo "ERROR\n";
		}
	}
?>
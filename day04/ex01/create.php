<?php

	if (!file_exists("../private/passwd")) {
		if (!file_exists("../private")) {
			mkdir("../private");
		}
		$content = "";
	}
	else
		$content = unserialize(file_get_contents("../private/passwd"));

	if ($_POST['submit'] != 'OK' || $_POST['passwd'] == "" || $content[$_POST['login']] != NULL) {
		echo "ERROR\n";
	}
	else {
		$content[$_POST['login']] = array('login' => $_POST['login'], 'passwd' => hash("whirlpool", $_POST['passwd']));
		file_put_contents("../private/passwd", serialize($content));
		echo "OK\n";
	}
?>
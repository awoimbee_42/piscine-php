<?php
class NightsWatch {
	private $fighters;

	public function __construct() {
		$this->fighters = array();
	}

	public function recruit($fighter) {
		if ($fighter instanceof IFighter)
			$this->fighters[] = $fighter;
	}

	public function fight() {
		foreach ($this->fighters as $f) {
			$f->fight();
		}
	}
}
?>
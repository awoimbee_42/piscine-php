<?php
class UnholyFactory {
	private $has_absorbed;

	public function __construct() {
		$this->has_absorbed = array();
	}

	public function absorb($fighter) {
		if (is_a($fighter, 'Fighter')) {
			print("(Factory absorbed a fighter of type ".$fighter->type.")".PHP_EOL);
			$this->has_absorbed[$fighter->type] = $fighter;
		}
		else
			print("(Factory can't absorb this, it's not a fighter)".PHP_EOL);
	}

	public function fabricate($fighter_type) {
		if ($this->has_absorbed[$fighter_type] === null) {
			print("(Factory hasn't absorbed any fighter of type $fighter_type)".PHP_EOL);
			return (null);
		}
		else {
			print("(Factory fabricates a fighter of type $fighter_type)".PHP_EOL);

			return ($this->has_absorbed[$fighter_type]);
		}
	}
}
?>
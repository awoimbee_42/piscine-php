<?php
class Jaime extends Lannister {
	public function sleepWith($person) {
		$name = get_class($person);
		switch ($name) {
			case "Tyrion":
				$this->nope();
				break;
			case "Sansa":
				$this->ok();
				break;
			case "Cersei":
				$this->pleasure();
				break;
		}
	}
}
?>
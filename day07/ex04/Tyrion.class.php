<?php
class Tyrion extends Lannister {
	public function sleepWith($person) {
		$name = get_class($person);
		switch ($name) {
			case "Jaime":
				$this->nope();
				break;
			case "Sansa":
				$this->ok();
				break;
			case "Cersei":
				$this->nope();
				break;
		}
	}
}
?>
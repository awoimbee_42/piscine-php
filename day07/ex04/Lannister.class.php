<?php
class Lannister {
	public function nope() {
		print("Not even if I'm drunk !".PHP_EOL);
	}
	public function ok() {
		print("Let's do this.".PHP_EOL);
	}
	public function pleasure() {
		print("With pleasure, but only in a tower in Winterfell, then.".PHP_EOL);
	}
}
?>
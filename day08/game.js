const requestPHP = (url, data, callback) => {
	// put data in POST format
	let postData = Object.keys(data).map( (k) => {
		return encodeURIComponent(k)+'='+encodeURIComponent(data[k])
		}).join('&');
	let xhttp = new XMLHttpRequest();
	xhttp.open("POST", url, true);
	xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200)
			callback(this.response);
		else if (this.readyState == 4)
			console.log("An error occured in the AJAX request...");
	};
	xhttp.send(postData);
}

const requestGamestate = () => {
	requestPHP('process_game_turn.php', {'curr': 1}, printBoard);
}

const printBoard = (state) => {
	g_board_data = JSON.parse(state);
	console.log(g_board_data);
	let data = g_board_data;

	let html = "";
	for(let y = 0; y <= data['dims']['y']; y++) {
		html += '<div class="line">';
		for(let x = 0; x <= data['dims']['x']; x++) {
		   html += '<div class="tile">' + '</div>';
		}
		html += '</div>';
	}
	let node = document.createElement('div');
	node.innerHTML = html;

	for (const debris of data['debris']) {
		for (let x = 0; x < debris['dims'].w; x++) {
			for (let y = 0; y < debris['dims'].h; y++) {
				let tile = node.children[y + debris['pos'].y].children[x + debris['pos'].x];
				// console.log(node.children[y + debris['pos'].y]);
				tile.className += ' debris';
			}
		}
	}

	for (const [play_name, play] of Object.entries(data['players'])) {

		for (const [id, ship] of play.entries()) {
			for (let x = 0; x < ship['dims'].w; x++) {
				for (let y = 0; y < ship['dims'].h; y++) {
					let tile = node.children[y + ship['pos'].y].children[x + ship['pos'].x];
					tile.className += " "+play_name;
					// tile.addEventListener("click", clickShip, true, id);
					tile.onclick = function() {clickShip(this, play_name, id)};
				}
			}
		}
	}
	document.getElementById("gameBoard").appendChild(node);
}

const shipAdd = function (type, player, ship_id) {
	let ship = g_board_data.players[player][ship_id];
	if (ship.pp != 0) {
		switch (type) {
			case "life":
				ship.life += 1;
				break;
			case "shield":
				ship.shield += 1;
				break;
			case "speed":
				ship.speed += 1;
				break;
		}
		ship.pp -= 1;
		requestPHP("process_game_turn.php", {"usePP":type, "player":player, "ship":ship_id}, ()=>{});
		clickShip(this, player, ship_id);
	}
}

const clickShip = (node, play_name, id) => {
	const orders = () => {
		html += "<h3>Use your "+ship.pp+" PowerPoints</h3>"
			+ "<button onclick='shipAdd(\"life\", \""+play_name+"\", "+id+")'>More Life</button>"
			+ "<button onclick='shipAdd(\"shield\", \""+play_name+"\", "+id+")'>More Shield</button>"
			+ "<button onclick='shipAdd(\"speed\", \""+play_name+"\", "+id+")'>More Speed</button>";
	}
	const movements = () => {

	}
	const shoot = () => {

	}

	// get controls for ship
	console.log(node);
	console.log(play_name);
	console.log(id);
	console.log(g_board_data.players[play_name]);

	let ctrl_node = document.getElementById("controls");

	let ship = g_board_data.players[play_name][id];
	let html = "<div>"
		+ "<h3 id='shipName'>" + ship.name + "</h3>"
		+ "<pre id='PPnb'>" + ship.pp + "</pre>"
		+ "<pre id='lifeStat'>" + ship.life + "</pre>"
		+ "<pre id='shieldStat'>" + ship.shield + "</pre>"
		+ "<pre id='speedStat'>" + ship.speed + "</pre>"
		+ "<pre id='manStat'>" + ship.man + "</pre>";
	if (typeof ship.status === 'undefined')
		orders();
	else if (ship.status === 1)
		movements();
	else if (ship.status === 2)
		shoot();
	html += "</div>";

	ctrl_node.innerHTML = html;
	// ctrl_node.appendChild(node);
}


// const add = (text) => {
// 	let ftlist = document.getElementById("ft_list");
// 	let node = document.createElement('div');
// 	node.innerHTML = text;
// 	node.onclick = maybe_delete;
// 	ftlist.insertBefore(node, ftlist.firstChild);
// }



// const setupGame = (app, gameBoard) => {

// }

// const changeState = (xhttp) => {
// 	data = xhttp.responseText;

// }


<?php
include_once('Thing.class.php');
include_once('Player.class.php');
include_once('Ray.class.php');

class Board {
	private $_dims;
	private $_debris;
	private $_players;
	private $_turn_nb;
	private $_curr_turn_play;

	const MAX_DIST = 9999;

	public function __toString() {
		return ("Map: w={$this->_dims['x']}, h={$this->_dims['y']}, (...)".PHP_EOL);
	}

	public function usePP() {

	}

	public function getBoard() {
		$map_content = [];

		$map_content['dims'] = $this->_dims;
		foreach ($this->_players as $play) {
			$ships = $play->getShips();
			foreach ($ships as $ship) {
				$map_content['players'][$play->getName()][] = $ship->getThis();
			}
		}
		foreach($this->_debris as $deb) {
				$map_content['debris'][] = $deb->getData();
		}
		return($map_content);
	}

	public function playShip(string $play_name, int $ship_id) {
		if ($play_name == $this->_curr_play_name) {
			return $this->_players[$play_name]->getShip($ship_id);
		}
	}

	public function __construct(array $args) {
		$this->_dims = array('x' => 150, 'y' => 100);
		foreach($args['debris'] as $deb) {
			$this->_debris[] = new Thing($deb);
		}
		foreach ($args['players'] as $player) {
			if (isset($this->_players[$player['name']]))
				die("player already exists");
			$this->_players[$player['name']] = new Player($player);
		}
		$this->_turn_nb = 0;
	}

	public function rayCollision(Ray $ray) {
		$closest_thing = FALSE;
		$closest_dist = self::MAX_DIST;
		$tmp_dist;

		foreach($this->_debris as $thing) {
			$tmp_dist = $thing->hitDist();
			if ($tmp_dist !== FALSE && $tmp_dist < $closest_dist) {
				$closest_thing = $thing;
				$closest_dist = $tmp_dist;
			}
		}
		foreach($this->_debris as $p) {
			foreach($p->getShips() as $ship)
			$tmp_dist = $ship->hitDist();
			if ($tmp_dist !== FALSE && $tmp_dist < $closest_dist) {
				$closest_thing = $ship;
				$closest_dist = $tmp_dist;
			}
		}
		if (method_exists($closest_thing, "gotHit")) {
			$closest_thing->gotHit($ray->getPower());
		}
	}


}

?>
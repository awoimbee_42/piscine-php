<?php
include_once('Spaceship.class.php');
class Player {
	private $_name;
	private $_ships;

	public function getShips() {
		return ($this->_ships);
	}
	public function playShip($shipId) {
		return ($this->_ships[$shipId]);
	}
	public function getName() {
		return ($this->_name);
	}

	public function __construct(array $args) {
		if (!isset($args['name']) || !isset($args['ships']))
			die("Player: not eough aguments !");
		$this->_name = $args['name'];
		foreach ($args['ships'] as $nwship) {
			$this->_ships[] = new Spaceship($nwship);
		}
	}

}

?>
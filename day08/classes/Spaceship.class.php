<?php
include_once('Thing.class.php');

interface Spaceship {
	public function getThis();

}

class Spaceship extends Thing {
	private $name;
	private $life;
	private $pp;
	private $shield;
	private $speed;
	private $maneuverability;
	private $gun;


	public function __construct(array $kwarg) {
		try {
			Thing::__construct($kwarg);
			$this->name = $kwarg['name'];

			$this->life = $kwarg['life'];
			$this->pp = $kwarg['pp'];
			$this->shield = $kwarg['shield'];
			$this->speed = $kwarg['speed'];
			$this->maneuverability = $kwarg['maneuverability'];
		}
		catch (OutOfBoundsException $exception) {
			die("ERROR (in spaceship constructor)");
		}
	}

	public function getThis() {
		return (array('name' => $this->name, 'dims' => $this->getDims(), 'pos'=>$this->getPos(), 'sprite'=>$this->getTEx(), 'life'=>$this->life, 'pp'=>$this->pp, 'shield'=>$this->shield, 'speed'=>$this->shield, 'speed'=>$this->speed, 'man'=>$this->maneuverability));
	}

	public function gotHit($damage) {
		if ($this->shield > 0) {
			$this->shield -= $damage;
		}
		else
			$this->life -= $damage;
	}

}

?>
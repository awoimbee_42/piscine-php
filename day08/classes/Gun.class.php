<?php
abstract class Gun {
	protected $dir; // <- == 0, ^ == 1, -> == 2, dwn == 3
	protected $pos; // relative to ship pos
	protected $sprite; // RESERVED

	const DIR_LFT = 0;
	const DIR_UP = 1;
	const DIR_RGT = 2;
	const DIR_DWN = 3;

	public function __construct(int $dir, array $pos, string $gun_name) {
		$this->dir = $dir;
		$this->pos = $pos;
	}

	abstract public function shoot();
}

?>
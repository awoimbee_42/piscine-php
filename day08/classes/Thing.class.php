<?php

class Thing {
	private $pos;
	private $dims;
	private $tex;

	public function __construct(array $kwargs) {
		$this->pos = $kwargs['pos'];
		$this->dims = $kwargs['dims'];
		$this->tex = $kwargs['tex'];
	}

	public function getPos() {
		return $this->pos;
	}
	public function getDims() {
		return $this->dims;
	}
	public function getTex() {
		return $this->tex;
	}
	public function getData() {
		return array(
			'pos'=>$this->pos,
			'dims'=>$this->dims,
			'sprite'=>$this->tex
		);
	}

	private function onSameLine(array $coo) {
		if ($this->pos['y'] <= $coo['y'] && $coo['y'] <= $this->pos['y'] + $this->dims['y'])
			return (TRUE);
		return (FALSE);
	}
	private function onSameColumn(array $coo) {
		if ($this->pos['x'] <= $coo['x'] && $coo['x'] <= $this->pos['x'] + $this->dims['x'])
			return (TRUE);
		return (FALSE);
	}
	public function hitDist($ray) {
		$rdir = $ray->getDir();
		$rorg = $ray->getOrg();
		if (onSameLine($rorg)) {
			if ($rdir === Ray::DIR_LFT && $rorg['x'] > $this->pos['x']) {
				return $rorg['x'] - ($this->pos['x'] + $this->dims['w']);
			}
			else if ($rdir === Ray::DIR_RGT && $rorg['x'] < $this->pos['x']) {
				return ($this->pos['x']) - $rorg['x'];
			}
		}
		else if (onSameColumn($rorg)) {
			if ($rdir === Ray::DIR_UP && $rorg['y'] > $this->pos['y']) {
				return $rorg['y'] - ($this->pos['y'] + $this->dims['h']);
			}
			else if ($rdir === Ray::DIR_DWN && $rorg['y'] < $this->pos['y']) {
				return ($this->pos['y']) - $rorg['y'];
			}
		}
		else
			return FALSE;
	}
}

?>
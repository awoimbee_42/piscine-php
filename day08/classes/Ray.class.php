<?php
class Ray {
	private $org;
	private $dir;
	private $power;

	const DIR_LFT = 0;
	const DIR_UP = 1;
	const DIR_RGT = 2;
	const DIR_DWN = 3;

	public function __construct(array $org, int $dir, int $power) {
		if (!isset($org['x']) || !isset($org['y']) || !isset($dir) | !isset($power))
			die ("/!\ Missing args in Ray constructor !");
		$this->org = $org;
		$this->dir = $dir;
		$this->power = $power;
	}

	public function getOrg() {
		return $this->org;
	}
	public function getDir() {
		return $this->dir;
	}
	public function getPower() {
		return $this->power;
	}
}

?>
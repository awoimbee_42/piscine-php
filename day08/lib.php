<?php
include_once('classes/Board.class.php');

function start_game() {
	$game_params = array(
		'debris' => array(
			array(
				'pos' => array('x' => 50, 'y' => 50),
				'dims' => array('w' => 5, 'h' => 5),
				'tex' => 'black',
			),
		),
		'players' => array(
			array(
				'name' => 'player1',
				'ships' => array(
					array(
						'name' => 'Rightful Clio 5',
						'pos' => array('x' => 1, 'y' => 2),
						'dims' => array('w' => 5, 'h' => 1),
						'tex' => 'black',
						'life' => 7,
						'pp' => 8,
						'shield' => 0,
						'speed' => 20,
						'maneuverability' => 2,
					),
					array(
						'name' => 'bite bleue',
						'pos' => array('x' => 50, 'y' => 3),
						'dims' => array('w' => 2, 'h' => 5),
						'tex' => 'blue',
						'life' => 5,
						'pp' => 5,
						'shield' => 0,
						'speed' => 20,
						'maneuverability' => 5,
					),
				),
			),
			array(
				'name' => 'player2',
				'ships' => array(
					array(
						'name' => 'Fucked Clio 5',
						'pos' => array('x' => 89, 'y' => 90),
						'dims' => array('w' => 5, 'h' => 1),
						'tex' => 'black',
						'life' => 7,
						'pp' => 8,
						'shield' => 0,
						'speed' => 20,
						'maneuverability' => 2,
					),
					array(
						'name' => 'bite pas bleue',
						'pos' => array('x' => 70, 'y' => 80),
						'dims' => array('w' => 2, 'h' => 5),
						'tex' => 'blue',
						'life' => 5,
						'pp' => 5,
						'shield' => 0,
						'speed' => 20,
						'maneuverability' => 5,
					),
				),
			)
		)
	);

	$_SESSION['game'] = new Board($game_params);
	$_SESSION['currPlay'] = 'player1';
}



?>
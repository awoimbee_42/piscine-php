#!/usr/bin/env php
<?php
	function ft_split($string) {
		$arr = preg_split('@ @', $string, NULL, PREG_SPLIT_NO_EMPTY);
		return ($arr);
	}
	function epur_str($string) {
		$stripped = trim(preg_replace('/(\s)+/', ' ', $string));
		if ($stripped != "")
			return ($stripped);
		return ("");
	}
	if ($argc > 1) {
		$truc = 0;
		$all_argv = $argv[1];
		$all_argv = epur_str($all_argv);
		$params_arr = ft_split($all_argv);
		array_push($params_arr, array_shift($params_arr));
		foreach ($params_arr as $p) {
			if ($truc == 0)
				echo $p;
			else
				echo " ", $p;
			$truc = 1;
		}
		echo "\n";
	}
?>

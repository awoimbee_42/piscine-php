#!/usr/bin/env php
<?php
	if ($argc == 2) {
		preg_match("/^\s*(?P<left>[0-9]+(?:.[0-9]*)?)\s*(?P<op>[+-\/*%]{1})\s*(?P<right>[0-9]+(?:.[0-9]*)?)\s*$/", $argv[1], $re);
		if (array_key_exists("left", $re) && array_key_exists("op", $re) && array_key_exists("right", $re)) {
			$left = floatval($re["left"]);
			$operator = $re["op"];
			$right = floatval($re["right"]);
			$result = 0;

			switch ($operator) {
				case "+":
					$result = $left + $right;
					break ;
				case "-":
					$result = $left - $right;
					break ;
				case "*":
					$result = $left * $right;
					break ;
				case "/":
					$result = $left / $right;
					break ;
				case "%":
					$result = $left % $right;
					break ;
				default:
					$result = 0;
			}
			echo $result, "\n";
		}
		else
			echo "Syntax Error\n";
	}
	else {
		echo "Incorrect Parameters\n";
	}
?>

#!/usr/bin/env php
<?php
$handle = fopen("php://stdin", "r");

while (true) {
	echo "Enter a number: ";
	$raw_input = fgets($handle);
	if ($raw_input == FALSE)
		break;
	$line = trim($raw_input);
	if (!preg_match("/^[+-]?[0-9]+$/", $line)) {
		echo "'", $line, "' is not a number\n";
	}
	else {
		$last_char = substr($line, -1);
		if ($last_char == "0" || $last_char == "2" || $last_char == "4" || $last_char == "6" || $last_char == "8")
			echo "The number ", $nb, " is even\n";
		else
			echo "The number ", $nb, " is odd\n";
	}
}
echo "\n";
fclose($handle);
?>
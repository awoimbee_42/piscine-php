#!/usr/bin/env php
<?php

	if ($argc == 4) {

		$left = floatval(trim($argv[1]));
		$operator = trim($argv[2]);
		$right = floatval(trim($argv[3]));
		$result = 0;

		switch ($operator) {
			case "+":
				$result = $left + $right;
				break ;
			case "-":
				$result = $left - $right;
				break ;
			case "*":
				$result = $left * $right;
				break ;
			case "/":
				$result = $left / $right;
				break ;
			case "%":
				$result = $left % $right;
				break ;
			default:
				$result = 0;
		}

		echo $result, "\n";
	}
	else
		echo "Incorrect Parameters\n";
?>

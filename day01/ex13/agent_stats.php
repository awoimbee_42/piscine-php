#!/usr/bin/env php
<?php
	// Usage:
	//		cat peer_notes_1.csv | ./agent_stats.php average
	//		cat peer_notes_1.csv | ./agent_stats.php average_user
	//		cat peer_notes_1.csv | ./agent_stats.php moulinette_variance

	if ($argv[1] === "average" || $argv[1] === "average_user" || $argv[1] === "moulinette_variance") {
		$handle = fopen("php://stdin", "r");

		fgetcsv ($handle, 100, ";" ); // remove first line

		if ($argv[1] === "average") {
			$average = [];
			while ($data = fgetcsv ($handle, 100, ";" )) {
				if ($data[2] !== "moulinette" && $data[1] !== "") {
					$average["ave"] += intval($data[1]);
					$average["gnb"] += 1;
				}
			}
			if ($average["gnb"]) {
				$average["ave"] /= $average["gnb"];
				echo $average["ave"], "\n";
			}
		}
		else if ($argv[1] === "average_user") {
			$averages = [];
			while ($data = fgetcsv ($handle, 100, ";" )) {
				if ($data[1] !== "" && $data[2] !== "moulinette") {
					$averages[$data[0]]["ave"] += intval($data[1]);
					$averages[$data[0]]["gnb"] += 1;
				}
			}
			ksort($averages);
			foreach ($averages as $user=>$grades) {
				echo $user, ":", ($grades["ave"] / $grades["gnb"]), "\n";
			}
		}
		else if ($argv[1] === "moulinette_variance") {
			$averages = [];
			while ($data = fgetcsv ($handle, 100, ";" )) {
				if ($data[1] !== "") {
					if ($data[2] === "moulinette") {
						$averages[$data[0]]["mave"] += intval($data[1]);
						$averages[$data[0]]["mnb"] += 1;
					}
					else {
						$averages[$data[0]]["ave"] += intval($data[1]);
						$averages[$data[0]]["gnb"] += 1;
					}
				}
			}
			ksort($averages);
			foreach ($averages as $user=>$grades) {
				$moul_ave = $grades["mave"] / $grades["mnb"];
				$usr_ave = $grades["ave"] / $grades["gnb"];
				$diff = ($usr_ave - $moul_ave);
				echo $user, ":", $diff, "\n";
			}
		}
		fclose($handle);
	}
?>

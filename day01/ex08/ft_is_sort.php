#!/usr/bin/env php
<?php
	function ft_is_sort($data) {
		$sorted = $data;
		sort($sorted);
		foreach ($sorted as $key=>$val) {
			if ($val != $data[$key]) {
				return (false);
			}
		}
		return (true);
	}
?>

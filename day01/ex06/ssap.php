#!/usr/bin/env php
<?php
	function ft_split($string) {
		$arr = preg_split('@ @', $string, NULL, PREG_SPLIT_NO_EMPTY);
		sort($arr);
		return ($arr);
	}
	function epur_str($string) {
		$stripped = trim(preg_replace('/(\s)+/', ' ', $string));
		if ($stripped != "")
			return ($stripped);
		return ("");
	}

	$all_argv = "";
	for ($i = 1; $i < $argc; $i++) {
		$all_argv .= $argv[$i] . " ";
	}

	$all_argv = epur_str($all_argv);
	$params_arr = ft_split($all_argv);
	foreach ($params_arr as $p)
		echo $p, "\n";
?>

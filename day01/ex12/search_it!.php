#!/usr/bin/env php
<?php

	if ($argc > 2) {
		$key = $argv[1];
		array_shift($argv);
		array_shift($argv);
		foreach ($argv as $val) {
			if (preg_match('/^' . $key . '[:]{1}/', $val)) {
				preg_match("/^" . $key . "[:]{1}(?P<value>.*)$/", $val, $value);
				echo $value["value"], "\n";
			}
		}
	}
?>

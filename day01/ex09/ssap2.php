#!/usr/bin/env php
<?php
	function epur_str($string) {
		$stripped = trim(preg_replace('/(\s)+/', ' ', $string));
		if ($stripped != "")
			return ($stripped);
		return ("");
	}
	function ft_split($string) {
		$arr = preg_split('@ @', $string, NULL, PREG_SPLIT_NO_EMPTY);
		sort($arr);
		return ($arr);
	}
	function is_lowletter($c) {
		if ('a' <= $c && $c <= 'z')
			return (TRUE);
		else
			return (FALSE);
	}
	function is_mydigit($c) {
		if ('0' <= $c && $c <= '9')
			return (TRUE);
		else
			return (FALSE);
	}
	function cmp($a, $b)
	{
		$a = strtolower((string)$a);
		$b = strtolower((string)$b);
		$maxlen = (strlen($a) < strlen($b) ? strlen($a) : strlen($b));

		for ($i = 0; $i < $maxlen; $i++) {
			if ($a[$i] == $b[$i])
				continue;
			if (is_lowletter($a[$i])) {
				if (is_lowletter($b[$i])) {
					if ($a[$i] < $b[$i])
						return (-1);
					else if ($a[$i] > $b[$i])
						return (1);
					continue;
				}
				else
					return (-1);
			}
			else if (is_lowletter($b[$i])) {
				return (1);
			}
			else if (is_mydigit($a[$i])) {
				if (is_mydigit($b[$i])) {
					if ($a[$i] < $b[$i])
						return (-1);
					else if ($a[$i] > $b[$i])
						return (1);
					continue;
				}
				else
					return (-1);
			}
			else if (is_mydigit($b[$i])) {
				return (1);
			}
			else {
				if ($a[$i] < $b[$i])
					return (-1);
				else if ($a[$i] > $b[$i])
					return (1);
			}
		}
		if (strlen($a) > $maxlen)
			return (1);
		else if (strlen($b) > $maxlen)
			return (-1);
		return (0);
	}

	if ($argc != 1)
	{
		$all_argv = "";
		for ($i = 1; $i < $argc; $i++) {
			$all_argv .= $argv[$i] . " ";
		}
		$all_argv = epur_str($all_argv);
		$words = ft_split($all_argv);

		usort($words, cmp);
		foreach($words as $w)
			echo $w, "\n";
	}
?>

<?php
    include_once('lib.php');
    session_start();
    $conn = sql_connect();
    $res = read_table($conn, "SELECT * FROM `Users` ORDER BY id DESC");
    while ($usr = mysqli_fetch_assoc($res))
    {
        if ($usr['email'] == $_SESSION['loggued_on_user']){
            $droit = $usr['root_access'];
            break ;
        }
    }
    if (isset($_SESSION['loggued_on_user']) && !empty($_SESSION['loggued_on_user']) and $droit == 1)
    {
        $res = read_table($conn, "SELECT * FROM `Users` ORDER BY id DESC");
?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="admin.css">
    </head>
    <body>
    <table>
            <tr>
                <td>Nom</td>
                <td>Prenom</td>
                <td>Mail</td>
                <td>Adresse</td>
                <td>Droit admin</td>
                <td>Action</td>
            </tr>
            <?php
            while ($user = mysqli_fetch_assoc($res))
            {
                ?>
                <tr>
                    <td><?= $user['name']; ?></td>
                    <td><?= $user['surname']; ?></td>
                    <td><?= $user['email']; ?></td>
                    <td><?= $user['address']; ?></td>
                    <td><a href="update_root.php?id=<?= $user['id']; ?>&root=<?= $user['root_access']; ?>"><?= $user['root_access']; ?></td>
                    <td><a href="supp_user.php?login=<?= $user['id']; ?>">supprimer</a></td>
                </tr>
                <?php
                $i++;
            }
?>
</table>
<table>
    <tr><th>Creer un utilisateur</th></tr>
    <tr>
        <td>Mail/Login</td>
        <td>Passwd</td>
        <td>Nom</td>
        <td>Prenom</td>
        <td>Adresse</td>
        <td>Droit Admin</td>
    </tr>
    <tr>
            <form action="add_user.php" method="post">
                <td><input required maxlength="64" name="login" placeholder="address email"></td>
                <td><input required maxlength="64" name="passwd" placeholder="mot de passe" /></td>
                <td><input required maxlength="64" name="name" placeholder="nom" /></td>
                <td><input required maxlength="64" name="surname" placeholder="prenom" /></td>
                <td><input required maxlength="64" name="address" placeholder="adresse" /></td>
                <td><input required max="1" min="0" name="root_access" type="number" value="0"/></td>
    </tr>
    <tr>
    <td><input class="btn" type="submit" name="submit" value="Créer votre compte"/></td>
    </tr>
            </form>

</table>
</body>
</html>
<?php
    mysqli_close($conn);
}
else
    header("Location:index.php");
?>
<?php
    include_once('lib.php');

    session_start();

    $auth = "";
    if ($_POST['submit'] === "Créer votre compte") {
        $login = $_POST['login'];
        $passwd = $_POST['passwd'];
        if (preg_match('/[^\'`]*@[^\'`]*\.[^\'`]*/', $_POST['login']) === 0)
            $auth = "Invalid email address";
        else if (trim($passwd) === "")
            $auth = "Invalid password";
        else {
            $auth = user_create($login, $passwd);
            if ($auth === TRUE) {
                $_SESSION['loggued_on_user'] = $login;
                header("Location: index.php");
            }
        }
    }
?>
<html>
<head>
    <?php include('head.php') ?>
    <title>Login</title>
    <meta charset="utf-8">
</head>
<body>
    <?php include('nav.php') ?>
    <div class="master_form">
        <div class="form">
            <h2>CONNECTION</h2>
            <hr>
            <br />
            <form method="post">
                <input type="email" required maxlength="64" name="login" placeholder="address email" />
                <br />
                <input type="password" required maxlength="64" name="passwd" placeholder="mot de passe" />
                <?php echo $auth; ?>
                <br />
                <h3>Informations complémentaires (optionel) :</h3>
                <input maxlength="64" name="name" placeholder="Nom" /><br />
                <input maxlength="64" name="surname" placeholder="Prénom" /><br />
                <textarea maxlength="300" name="address" placeholder="Address" rows="5" autocorrect='on' autocapitalize='on' spellcheck='on' maxlength="350" cols="40"></textarea><br />
                <br />
                <input class="btn" type="submit" name="submit" value="Créer votre compte"/>
            </form>
            <a href="./create.php">Pas encore de compte ?</a>
            <a href="./modif.php">Envie de changer de mot de passe ?</a>
        </div>
    </div>
</body>
</html>

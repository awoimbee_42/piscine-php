<?php
include_once("lib.php");
session_start();
$product_id = intval($_GET['id']);
$sql_conn = sql_connect();
$product_sql = read_table($sql_conn, "select * from `products` where `id`='$product_id'");
// while(($product_arr = mysqli_fetch_assoc($product_sql)))
//
if (!($product_arr = mysqli_fetch_assoc($product_sql)))
	error('Product doesn\'t exist 👻');
// var_dump($product_arr);
mysqli_close($sql_conn);
$product_arr['note'] = number_format((($product_arr['notes_nb'] == 0) ? 0 : $product_arr['notes_full'] / $product_arr['notes_nb']), 2);
$product_arr['pictures'] = unserialize($product_arr['pictures']);
?>

<html>
    <head>
        <?php include('head.php') ?>
    </head>
    <body>
		<?php include('nav.php') ?>
		<div id="item_page">
			<h2><?php echo $product_arr["name"]; ?></h2>
			<i class='item-note'><?php echo $product_arr['note']; ?>/5 (<?php echo $product_arr['notes_nb']; ?> notes)</i>
			<div class="img_carousel">
				<?php
				$p_nb = 0;
				foreach($product_arr['pictures'] as $img) {
					echo "<div class='img'><img src='$img' /></div>\n";
					$p_nb += 1;
				}
				while ($p_nb < 5) {                   //carousel needs 4 ehm, 5 images to work correctly so....
					echo "<div class='img'><img src='$img' /></div>\n";
					$p_nb++;
				}
				?>
			</div>
			<br />
			<i style="color:red;">Prix: <?php echo $product_arr['price']; ?>€</i>
			<a style="add_cart" href="cart.php?add=<?php echo $product_arr['id']; ?>">Add to cart</a>
			<p><?php echo $product_arr["description"]; ?></p>

		</div>

    </body>
</html>
<?php
	include('lib.php');

	$hints = "<br />Pleade add 'ErrorDocument 404 /rush00/error.php?err=404' to you vhost or .htaccess"
			."<br />Please comment 'skip-name-resolve=1' in your mysql config file";

	$params = get_sql_parameters();
	$conn = mysqli_connect($params['dburl'], $params['login'], $params['passwd']);
	if ($conn === FALSE)
		error('Could not connect: ('.mysqli_connect_errno().')'.mysqli_connect_error(). ', '.mysqli_error($conn).$hints);

	$query = "DROP DATABASE IF EXISTS `rush00`";
	if (mysqli_query($conn, $query) === FALSE)
		error("Error cleaning database: " . mysqli_error($conn).$hints);

	$query = "CREATE DATABASE IF NOT EXISTS `{$params['dbname']}`";
	if (mysqli_query($conn, $query) === FALSE)
		error("Error creating database: " . mysqli_error($conn).$hints);
	if (mysqli_select_db($conn, $params['dbname']) === FALSE)
		error("Error selecting database: " . mysqli_error($conn).$hints);

	$query = "CREATE TABLE `products` (
		`id` INT NOT NULL AUTO_INCREMENT,
		`inventory` INT NULL DEFAULT NULL,
		`name` TINYTEXT NULL DEFAULT NULL,
		`description` TEXT NULL DEFAULT NULL,
		`pictures` TEXT NULL DEFAULT NULL,
		`price` FLOAT NULL DEFAULT NULL,
		`notes_full` INT NULL DEFAULT NULL,
		`notes_nb` INT NULL DEFAULT NULL,
		`color_id` BOOLEAN NULL DEFAULT NULL,
		`cordless` BOOLEAN NULL DEFAULT NULL,
		`mic` BOOLEAN NULL DEFAULT NULL,
		`kids` BOOLEAN NULL DEFAULT NULL,
		`gaming` BOOLEAN NULL DEFAULT NULL,
		`in-ear` BOOLEAN NULL DEFAULT NULL,
		`over-ear` BOOLEAN NULL DEFAULT NULL,
		`around-ear` BOOLEAN NULL DEFAULT NULL,
		PRIMARY KEY (`id`)
	  ) ENGINE = InnoDB;";
	if (mysqli_query($conn, $query) === FALSE)
		error("Error creating table: " . mysqli_error($conn).$hints);

	$products = array(
		array('inventory' => 10, 'name' => 'airpods',
		'description' => '<h3>Plus magiques que jamais.</h3>Temps de conversation augmenté, activation vocale de Siri et nouveau boîtier de charge sans fil. Les nouveaux AirPods sont des écouteurs sans fil, et sans commune mesure. Retirez-les de leur boîtier et ils fonctionnent tout de suite avec tous vos appareils. Portez-les à vos oreilles et ils se connectent immédiatement pour vous faire profiter d’un son de haute qualité parfaitement détaillé. Plus que géniaux, ils sont magiques.',
		'pictures' => serialize(array('imgs/airpods.jpg')),'price' => 229.99,'notes_full' => 100,'notes_nb' => 60,
		'cordless' => 1, 'mic' => 1,'kids' => 0, 'gaming' => 0, 'in-ear' => 1,'around-ear' => 0, 'color-id' => 6),

		array('inventory' => 5, 'name' => 'senheiser HD 660 S Apogee',
		'description' => "Pour les fichiers musicaux lus sur des PC et des Mac ou depuis des services de type Spotify, Google Play, Apple Music ou Tidal, la sortie de l'ordinateur peut s'avérer être un goulot d'étranglement limitant sa capacité à fournir une puissance et une qualité suffisantes à un casque de qualité audiophile.
					<br />Plus maintenant : la nouvelle offre groupée de Sennheiser réunit le casque dynamique ouvert HD 660 S, haut de gamme de la marque, et l'Apogee Groove, convertisseur N/A USB et amplificateur pour casque. Cette combinaison permet à un utilisateur de PC ou de Mac de profiter de l'intégralité de la plage dynamique et des niveaux de distorsion ultra-faibles de l'un des meilleurs casques Sennheiser de tous les temps.
					<br />Dans ce nouvel ensemble, l'expertise audio haut de gamme de Sennheiser est parfaitement complétée par les 30 ans d'innovation d'Apogee comme leader du secteur de l'enregistrement audio numérique. Les artistes, producteurs et ingénieurs du son les plus célèbres du monde s'appuient sur les produits Apogee pour des prestations récompensées aux GRAMMY Awards.",
		'pictures' => serialize(array('imgs/HD660SAPOGEE01.jpg', 'imgs/HD660SAPOGEE02.jpg', 'imgs/HD660SAPOGEE03.jpg')),'price' => 549,'notes_full' => 500,'notes_nb' => 100,
		'cordless' => 0, 'mic' => 0,'kids' => 0, 'gaming' => 0, 'in-ear' => 0,'around-ear' => 1, 'color-id' => 1),

		array('inventory' => 5, 'name' => 'Casque sans fil QuietComfort 35 II',
		'description' => "Réduction de bruit ajustable, assistants vocaux intégrés, jusqu’à 20 heures d’autonomiebose",
		'pictures' => serialize(array('imgs/bose1.jpeg', 'imgs/bose2.jpg', 'imgs/bose3.jpg')),'price' => 379,'notes_full' => 700,'notes_nb' => 150,
		'cordless' => 1, 'mic' => 1, 'kids' => 0, 'gaming' => 0, 'in-ear' => 0,'around-ear' => 1, 'color-id' => 1),

		array('inventory' => 50, 'name' => 'SELECLINE JY E812',
		'description' => "Ecoutez votre musique partout où que vous soyez grâce à ces écouteurs. Dispose d'une prise Jack 3.5 mm vous pouvez les utiliser avec tous appareils équipés Jack 3.5 mm (smartphone, baladeur,...etc).",
		'pictures' => serialize(array('imgs/selecline1.jpeg')),'price' => 2,'notes_full' => 3,'notes_nb' => 1,
		'cordless' => 0, 'mic' => 0, 'kids' => 0, 'gaming' => 0, 'in-ear' => 1,'around-ear' => 0, 'color-id' => 1),

		array('inventory' => 30, 'name' => 'iclever rose',
		'description' => "Limitation du volume sécurisée-Circuit de limitation de volume intégré qui maintient la pression sonore à des niveaux recommandés pour les jeunes oreilles.
					<br />Un son supérieur entoure vos oreilles-Grâce au son inimitable, profond, net, puissant, les écouteurs iclever enfant casque audio gèrent le son avec plus de précision que les autres, garantissant ainsi un son naturel.
					<br />Coussinets d'oreille doux au toucher-Le serre-tête pivotant et les oreillettes pivotantes le rendent non seulement plus durable et confortable, mais vous permettent également de profiter d'un festin de haute qualité après une longue écoute sans interruption.",
		'pictures' => serialize(array('imgs/iclever1.jpg', 'imgs/iclever2.jpg', 'imgs/iclever3.jpg')),'price' => 20,'notes_full' => 200,'notes_nb' => 10,
		'cordless' => 0, 'mic' => 0, 'kids' => 1, 'gaming' => 0, 'in-ear' => 0,'around-ear' => 1, 'color-id' => 5),

		array('inventory' => 30, 'name' => 'Beats X',
		'description' => "Conçus pour votre quotidien, les écouteurs sans fil BeatsX vous accompagnent partout. Avec 8 heures d'autonomie et dotés de la technologie Fast Fuel, ces écouteurs vous permettent de profiter d'un son clair et authentique toute la journée",
		'pictures' => serialize(array('imgs/beats2.jpg', 'imgs/beats1.jpeg')),'price' => 119,'notes_full' => 400,'notes_nb' => 120,
		'cordless' => 1, 'mic' => 1, 'kids' => 0, 'gaming' => 0, 'in-ear' => 1,'around-ear' => 0, 'color-id' => 1),

		array('inventory' => 30, 'name' => 'TEMIUM OH450',
		'description' => "Ce casque arceau de type supra aural dispose d'une bandeau large pour plus de confort et de maintien. 
		<br />Léger, vous le porterez aisément sans même vous rappeler que vous le portez. 
		<br />Ses oreillettes en mousse sont d'une douceur pour vos oreilles. 
		<br />Il est compatible avec tous les appareils munis d'une prise jack 3.5 mm.
		<br />Casque Pro Gameur !",
		'pictures' => serialize(array('imgs/temium2.jpg', 'imgs/temium1.jpg', 'imgs/temium3.jpg')),'price' => 750,'notes_full' => 5000,'notes_nb' => 1000,
		'cordless' => 0, 'mic' => 0, 'kids' => 0, 'gaming' => 1, 'in-ear' => 0,'around-ear' => 1, 'color-id' => 6),
	
		array('inventory' => 30, 'name' => 'BEATS EP BLEU',
		'description' => "Bénéficiez d'un son clair et puissant sans interruption avec le casque supra-auriculaire Beats EP. 
		<br />Le Beats EP est le choix idéal pour les mélomanes qui souhaitent profiter d'une  expérience d'écoute dynamique. 
		<br />Compact et léger vous ne sentirez pas sa présence. Ses  coussinets rembourrés se positionneront sur vos oreilles de manière à ne pas laisser s'échapper le son et à vous apporter le plus de douceur possible. 
		<br />Il offre de hautes performances sonores, une clarté du son maximale et produit des basses profondes et puissantes.
		<br />Robuste, léger et confortable avec sa structure en inox munie de glissières verticales qui se règlent pour un confort personnalisé.
		Profitez  d'une écoute sans interruption ! Le câble fixe vous permet de vous c oncentrer sans encombre sur la musique. Il vous suffit de l'emporter avec vous.",
		'pictures' => serialize(array('imgs/beatsbleu.jpg')),'price' => 70,'notes_full' => 460,'notes_nb' => 100,
		'cordless' => 0, 'mic' => 1, 'kids' => 0, 'gaming' => 0, 'in-ear' => 0,'around-ear' => 1, 'color-id' => 6),
	);

	foreach($products as $p) {
		$query = "INSERT INTO `products` (`id`,`inventory`,`name`,`description`,`pictures`,`price`,`notes_full`,`notes_nb`,`cordless`,`mic`,`kids`,`gaming`,`in-ear`,`over-ear`,`around-ear`,`color_id`)
				VALUES (NULL,'{$p['inventory']}','{$p['name']}',\"{$p['description']}\",'{$p['pictures']}','{$p['price']}','{$p['notes_full']}','{$p['notes_nb']}','{$p['cordless']}','{$p['mic']}','{$p['kids']}','{$p['gaming']}','{$p['in-ear']}','{$p['over-ear']}','{$p['around-ear']}','{$p['color-id']}');";
	if (mysqli_query($conn, $query) === FALSE)
		error("Error while adding product to table: " . mysqli_error($conn).$hints);

	}

	$passwd = hash("whirlpool", "root");
	$query = "CREATE TABLE `rush00`.`users` (
		`id` INT NOT NULL AUTO_INCREMENT,
		`email` TINYTEXT NOT NULL,
		`password` VARCHAR(128) NOT NULL,
		`name` TINYTEXT NOT NULL,
		`surname` TINYTEXT NOT NULL,
		`address` TEXT NOT NULL,
		`root_access` BOOLEAN NOT NULL DEFAULT FALSE,
		PRIMARY KEY (`id`)
	  ) ENGINE = InnoDB;";
	if (mysqli_query($conn, $query) === FALSE)
		error("Failed to create users table: " . mysqli_error($conn).$hints);
	$query = "INSERT INTO
		`users` (`id`,`email`,`password`,`name`,`surname`,`address`,`root_access`)
  		VALUES(NULL,'root@root.root','$passwd','root','root','/','1');";
	if (mysqli_query($conn, $query) === FALSE)
		error("Failed to create root user: " . mysqli_error($conn).$hints);

	$query = "CREATE TABLE `rush00`.`transactions` (
		`id` INT NOT NULL AUTO_INCREMENT,
		`item_id` INT NOT NULL,
		`user_id` INT NOT NULL,
		`date` TIMESTAMP NOT NULL,
		`shipping_company` SMALLINT NOT NULL,
		`delivery_address` TEXT NOT NULL,
		PRIMARY KEY (`id`)
	  ) ENGINE = InnoDB;";
	if (mysqli_query($conn, $query) === FALSE)
		error("Failed to create transactions table: " . mysqli_error($conn).$hints);

	mysqli_close($conn);
	echo "All Good !\n";
?>
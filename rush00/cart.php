<?php
include_once('lib.php');
session_start();

if (isset($_GET['delall'])) {
	$_SESSION['cart'] = array();
	header('Location: cart.php');
}

if (isset($_GET['validate'])) {
	$c = sql_connect();
	$t = read_table($c, "INSERT INTO `transactions` (`id`, `item_id`, `user_id`, `date`, `shipping_company`, `delivery_address`)
	VALUES(NULL,'1','1','1','1','{$_SESSION['loggued_on_user']}','/');");

	$_SESSION['cart'] = array();
	header('Location: cart.php');
}

if (isset($_GET['add'])) {
	if (!isset($_SESSION['cart'])) {
		$_SESSION['cart'] = array($_GET['add'] => 1);
	}
	else if (!isset($_SESSION['cart'][$_GET['add']])) {
		$_SESSION['cart'][$_GET['add']] = 1;
	}
	else {
		$_SESSION['cart'][$_GET['add']] += 1;
	}
	header('Location: cart.php');
}
if (isset($_GET['del'])) {
	if (isset($_SESSION['cart']) && isset($_SESSION['cart'][$_GET['del']])) {
		if ($_SESSION['cart'][$_GET['del']] === 1) {
			unset($_SESSION['cart'][$_GET['del']]);
		}
		else {
			$_SESSION['cart'][$_GET['del']] -= 1;
		}
	}
	header('Location: cart.php');
}

?>


<html>
<head>
	<?php include('head.php') ?>
</head>
<body>
	<?php include('nav.php'); ?>
	<div id="cart_main">
		<div class="cart">
			<h2 class="title">Mon panier</h2>

			<?php
			$ids = array();
			foreach($_SESSION['cart'] as $id=>$nb) {
				$ids[] = $id;
			}
			$products_info = get_cart_products($ids);
			$tot_price = 0;
			foreach($_SESSION['cart'] as $id=>$nb) {
				$img = unserialize($products_info[$id]['pictures'])[0];
				$p = $nb * $products_info[$id]['price'];
				$tot_price += $p;
				echo "<div class='item'>"
						."<div class='buttons'>"
							."<a href='cart.php?del=$id'>❌</a>"
						."</div>"
						."<div class='img'>"
							."<img src='$img' />"
						."</div>"
						."<h4 class='name'>{$products_info[$id]['name']}</h4>"
						."<p class='quantity'>$nb</p>"
						."<p class='unit-price'>{$products_info[$id]['price']}€</p>"
						."<h4 class='mult-price'>total: {$p}€</h4>"
					."</div>";
			}
			?>
			<h3 class='tot-price'>Grand total <?php echo $tot_price; ?>€</h3>
			<a href='cart.php?delall'>❌ vider le panier ❌</a>
			<a href='cart.php?validate'>VALIDER</a>

		</div>
	</div>


</body>
</html>

<?php
    include_once('lib.php');
    if(session_status() == PHP_SESSION_NONE){
        session_start();
    }
    if (!isset($_SESSION['loggued_on_user']) || $_SESSION['loggued_on_user'] == "") {
        echo "<nav id=\"page-head\">
                <div id=\"toolsbar\">
                    <a href=\"create.php\">Créer un compte</a>
                    <a href=\"login.php\">Se connecter</a>";
    }
    else {
        echo "<nav id=\"page-head\">
                <div id=\"toolsbar\">
                <a>{$_SESSION['loggued_on_user']}</a>
                <a href='./logout.php'>logout</a>";
    }
?>
        <a id="cart" href="cart.php"><img src="imgs/cart.png" /></a>
        <a id="site-logo" href="index.php">Acceuil</a>
    </div>
    <div id="navbar">
        <a href="categories.php?around-ear=1">Casques</a>
        <a href="categories.php?in-ear=1">Ecouteurs</a>
        <a href="categories.php?cordless=1">Sans fil</a>
        <a href="categories.php?cordless=0">Filaires</a>
    </div>
</nav>

<div class="freaking-cart">
    <h3>Panier:</h3>
    <?php

    $ids = array();
    foreach($_SESSION['cart'] as $id=>$nb) {
        $ids[] = $id;
    }

    $products_info = get_cart_products($ids);
    $tot_pice = 0;
    foreach($_SESSION['cart'] as $id=>$nb) {
        echo "<p>$nb {$products_info[$id]['name']} : {$products_info[$id]['price']}€</p>";
        $tot_pice += $nb * $products_info[$id]['price'];
    }
    echo "<h4>Total: {$tot_pice}€</h4>";

    ?>
</div>
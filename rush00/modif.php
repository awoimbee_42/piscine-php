<?php
include_once('lib.php');
session_start();

if (isset($_POST['submit']) && isset($_SESSION['loggued_on_user']) && log_usr_is_admin($_SESSION['loggued_on_user'])) {
    $id = $_POST['submit'];
    $conn = sql_connect();
    $res = read_table($conn, "UPDATE `products`
                        SET
                            `name` = '{$_POST['name']}',
                            `inventory` = '{$_POST['inventory']}',
                            `description` = '{$_POST['description']}',
                            `price` = '{$_POST['price']}',
                            `notes_full` = '{$_POST['notes_full']}',
                            `notes_nb` = '{$_POST['notes_nb']}',
                            `color_id` = '{$_POST['color_id']}',
                            `cordless` = '{$_POST['cordless']}',
                            `mic` = '{$_POST['mic']}',
                            `kids` = '{$_POST['kids']}',
                            `gaming` = '{$_POST['gaming']}',
                            `in-ear` = '{$_POST['in-ear']}',
                            `over-ear` = '{$_POST['over-ear']}',
                            `around-ear` = '{$_POST['around-ear']}'
                        WHERE
                            `id` = '$id';");
    header("Location: admin.php");
    // `pictures` = '{$_POST['pictures']}', PICTURES CANT BE UPDATED CAUSE F.
}

if (isset($_GET['id']) && isset($_SESSION['loggued_on_user'])) {
    $droit = log_usr_is_admin($_SESSION['loggued_on_user']);

    if ($droit === TRUE) {



        ?>
        <form action="modif.php" method="post">
            <table>
                <tr>
                    <td>Produit</td>
                    <td>Description</td>
                    <td>Prix</td>
                    <td>notes_full</td>
                    <td>notes_nb</td>
                    <td>catégorie</td>
                    <td>Supprimer</td>
                    <td>modifier</td>
                </tr>
            <?php
            $product = get_cart_products(array($_GET['id']));
            $product = array_values($product)[0];
                ?>

                <tr>
                    <td><input name="name" value="<?= $product['name']; ?>" /></td>
                    <td><input name="inventory" value="<?= $product['inventory']; ?>" /></td>
                    <td><input name="description" value="<?= $product['description']; ?>" /></td>
                    <td><input type="number" step="0.01" name="price" value="<?= $product['price']; ?>" /></td>
                    <td><input type="number" step="0.01" name="notes_full" value="<?= $product['notes_full']; ?>" /></td>
                    <td><input type="number" step="0.01" name="notes_nb" value="<?= $product['notes_nb']; ?>" /></td>
                    <td><input min="0" type="number" name="color_id" value="<?= $product['color_id']; ?>" /></td>
                    <td><input min="0" max="1" type="number" name="cordless" value="<?= $product['cordless']; ?>" /></td>
                    <td><input min="0" max="1" type="number" name="mic" value="<?= $product['mic']; ?>" /></td>
                    <td><input min="0" max="1" type="number" name="kids" value="<?= $product['kids']; ?>" /></td>
                    <td><input min="0" max="1" type="number" name="gaming" value="<?= $product['gaming']; ?>" /></td>
                    <td><input min="0" max="1" type="number" name="in-ear" value="<?= $product['in-ear']; ?>" /></td>
                    <td><input min="0" max="1" type="number" name="over-ear" value="<?= $product['over-ear']; ?>" /></td>
                    <td><input min="0" max="1" type="number" name="around-ear" value="<?= $product['around-ear']; ?>" /></td>
                </tr>
            </table>
            <button name="submit" value="<?= $_GET['id']; ?>" type="submit">modifier</button>
        </form>

            <?php

    }
}
?>
<?php
    include_once('lib.php');
    session_start();

    if (isset($_SESSION['loggued_on_user']) && !empty($_SESSION['loggued_on_user']))
    {
        $conn = sql_connect();
        $res = read_table($conn, "SELECT * FROM `products` ORDER BY id DESC");
?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="admin.css">
    </head>
    <body>
    <table>
            <tr>
                <td>Produit</td>
                <td>Description</td>
                <td>Prix</td>
                <td>notes_full</td>
                <td>notes_nb</td>
                <td>catégorie</td>
                <td>Supprimer</td>
                <td>modifier</td>
            </tr>
            <?php
            while ($product = mysqli_fetch_assoc($res))
            {
                ?>
                <tr>
                    <td><?= $product['name']; ?></td>
                    <td><?= $product['description']; ?></td>
                    <td><?= $product['price']; ?></td>
                    <td><?= $product['notes_full']; ?></td>
                    <td><?= $product['notes_nb']; ?></td>
                    <td><?php
                    if ($product['cordless'] == 1)
                        echo "cordless ";
                    if ($product['mic'] == 1)
                        echo "mic ";
                     if ($product['kids'] == 1)
                        echo "kids ";
                     if ($product['gaming'] == 1)
                        echo "gaming ";
                     if ($product['in-ear'] == 1)
                        echo "in-ear ";
                     if ($product['over-ear'] == 1)
                        echo "over-ear ";
                     if ($product['around-ear'] == 1)
                        echo "around-ear ";?></td>
                    <td><a href="suppr.php?id=<?= $product['id']; ?>">supprimer</a></td>
                    <td><a href="modif.php?id=<?= $product['id']; ?>">modifier</a></td>
                </tr>
                <?php
                // $i++;
            }
?>
</table>
<table>
    <tr><th>Ajouter un produit</th></tr>
    <tr>
        <td>Produit</td>
        <td>Description</td>
        <td>Prix</td>
        <td>notes_full</td>
        <td>notes_nb</td>
        <td>chemin image</td>
    </tr>
    <tr>
            <form action="add_produit.php" method="post">
                <td><input required maxlength="64" name="produit" placeholder="produit"></td>
                <td><input required maxlength="250" name="desc" placeholder="description" /></td>
                <td><input required min="1" name="prix" type="number"  placeholder="prix" /></td>
                <td><input required min="0" name="full" type="number"  placeholder="full" /></td>
                <td><input required min="0" name="nb" type="number"  placeholder="nb" /></td>
                <td><input required maxlenght name="img0" placeholder="img" /></td>
                <td><input maxlenght name="img1" placeholder="img" /></td>
                <td><input maxlenght name="img2" placeholder="img" /></td>
                <td><input maxlenght name="img3" placeholder="img" /></td>
    </tr>
    <tr>
        <td>cordless</td>
        <td>mic</td>
        <td>kids</td>
        <td>gaming</td>
        <td>in-ear</td>
        <td>over-ear</td>
        <td>around-ear</td>
    </tr>
    <tr>
                <td><input required min="0" max="1" value="0" name="cordless" type="number"  placeholder="nb" /></td>
                <td><input required min="0" max="1" value="0" name="mic" type="number"  placeholder="nb" /></td>
                <td><input required min="0" max="1" value="0" name="kid" type="number"  placeholder="nb" /></td>
                <td><input required min="0" max="1" value="0" name="gaming" type="number"  placeholder="nb" /></td>
                <td><input required min="0" max="1" value="0" name="in-ear" type="number"  placeholder="nb" /></td>
                <td><input required min="0" max="1" value="0" name="over-ear" type="number"  placeholder="nb" /></td>
                <td><input required min="0" max="1" value="0" name="around-ear" type="number"  placeholder="nb" /></td>
    </tr>
    <tr>
    <td><input class="btn" type="submit" name="submit" value="Ajouter Produit"/></td>
    </tr>
            </form>

</table>
</body>
</html>
</body>
</html>
<?php
    mysqli_close($conn);
}
else
    header("Location:index.php");
?>
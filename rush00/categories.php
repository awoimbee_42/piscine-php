<?php
    include_once("lib.php");
    $products_options = array();
    foreach($_GET as $option=>$value){
        if (preg_match('/[a-z]*/', $option) && preg_match('/[0-9]*/', $value))
            $products_options[$option] = $value;
    }

    function add_get_prams ($name, $value) {
        $url = "gategories.php?$name=$value";
        foreach ($products_options as $option=>$val) {
            $url .= "&$option=$val";
        }
    }

    $sql_conn = sql_connect();
    $sql_prod_cats = read_table($sql_conn, "select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='products'");
    $unselected_prod_cats = [];
    $selected_prod_cats = [];

    for ($i = 0; $i < 9; $i++) // skip the first 9 non modifiable 'categories', very wasteful, didnt find a better way (sql's OFFSET takes ages)
        mysqli_fetch_assoc($sql_prod_cats);
    while (($cat = mysqli_fetch_assoc($sql_prod_cats))) {
        $cat_name = $cat['COLUMN_NAME'];
        if (!isset($_GET[$cat_name]) || $_GET[$cat_name] === "0")
            array_push($unselected_prod_cats, $cat_name);
        else
            array_push($selected_prod_cats, $cat_name);
    }
?>

<html>
    <head>
        <?php include('head.php') ?>
    </head>

    <script>
        const addParams = (key, val, url) => {
            let arr = url.split('?');
            if(arr.length == 1) {
                window.location = url + '?' + key + '=' + val;
            }
            else if(arr.length == 2) {
                let params_arr = arr[1].split('&');
                let params_dict = {};
                let strarr = [];
                params_arr.forEach((elem, index) => {
                    let a = elem.split('=');
                    params_dict[a[0]] = a[1];
                });
                params_dict[key] = val;
                for(let key in params_dict) {
                    strarr.push(key + '=' + params_dict[key]);
                }
                let str = strarr.join('&');
                window.location = (arr[0] + '?' + str);
            }
        }
        const removeParams = (key, url) => {
            let arr = url.split('?');
            if (arr.length == 1) {
                window.location = (url);
            }
            else {
                let params = arr[1].split('&');
                let res_param_str = [];
                params = params.map((elem) => {
                    if (!elem.startsWith(key)) {
                        return (elem);
                    }
                });
                let str = params.filter(Boolean).join('&');
                window.location = (arr[0] + '?' + str);
            }
        }
        const changeParams = (key, newVal) => {
            let url = window.location.href;
            let arr = url.split('?');
            if (arr.length == 1) {
                addParams(key, newVal, url);
            }
            let did_it = false;
            let params = arr[1].split('&');
            let res_param_str = [];
            params = params.map((elem) => {
                if (!elem.startsWith(key)) {
                    return (elem);
                }
                else {
                    did_it = true;
                    return (key + '=' + newVal);
                }
            });
            let str = params.filter(Boolean).join('&');
            if (did_it)
                window.location = (arr[0] + '?' + str);
            else
                addParams(key, newVal, url);
        }

        const categories = <?php echo json_encode($prod_cats); ?>;
    </script>



    <body>
        <?php include('nav.php') ?>
        <div class="categories">
            <div class="dropdown">
                <button class="dropbtn color-picker">couleur</button>
                <ul class="dropdown-content">
                    <li><button class='' onclick='removeParams("color_id", window.location.href)'>toutes les couleurs</button></li>
                    <li><button class='' onclick='changeParams("color_id", 1)'>noir</button></li>
                    <li><button class='' onclick='changeParams("color_id", 2)'>rouge</button></li>
                    <li><button class='' onclick='changeParams("color_id", 3)'>vert</button></li>
                    <li><button class='' onclick='changeParams("color_id", 4)'>bleu</button></li>
                    <li><button class='' onclick='changeParams("color_id", 5)'>rose</button></li>
                    <li><button class='' onclick='changeParams("color_id", 6)'>blanc</button></li>
                </ul>
            </div>
            <div class="cat-unselected">
                <?php
                    foreach($unselected_prod_cats as $prod_cat) {
                        echo "<button class='prod_cat unselected' onclick='addParams(\"$prod_cat\", 1, window.location.href)'>$prod_cat</button>";
                    }
                ?>
            </div>
            <div class="spacer"></div>
            <div class="cat-selected">
                <?php
                    foreach($selected_prod_cats as $prod_cat) {
                        echo "<button class='prod_cat selected' onclick='removeParams(\"$prod_cat\", window.location.href)'>$prod_cat</button>";
                    }
                ?>
            </div>

        </div>
		<section class="products_view">
			<?php products_view($products_options); ?>
        </section>
    </body>
</html>
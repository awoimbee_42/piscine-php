<?php
session_start();
include_once('lib.php');

$auth = "";
$login = $_POST['login'];
$passwd = $_POST['passwd'];
$name = $_POST['name'];
$surname = $_POST['surname'];
$address = $_POST['address'];
$droit = $_POST['root_access'];
if ($_POST['submit'] === "Créer votre compte") {
    if (preg_match('/[^\'`]*@[^\'`]*\.[^\'`]*/', $_POST['login']) === 0)
        $auth = "Invalid email address";
    else if (trim($passwd) === "")
        $auth = "Invalid password";
    else {
        $auth = user_create($login, $passwd, $name, $surname, $address, $droit);

        if ($auth === TRUE) {
            header("Location:user.php");
        }
        else {
            echo $auth."\n";
        }
    }
}
?>
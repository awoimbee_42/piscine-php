
const requestPHP = (url, data) => {
	// put data in POST format
	let postData = Object.keys(data).map( (k) => {
		return encodeURIComponent(k)+'='+encodeURIComponent(data[k])
		}).join('&');
	let xhttp = new XMLHttpRequest();
	xhttp.open("POST", url, true);
	xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200)
			;
	};
	xhttp.send(postData);
}

function maybe_delete() {
	if (confirm("You sure you want do delete this ?")) {
		let d = this.innerHTML;
		requestPHP('delete.php', {'text': d});
		this.parentNode.removeChild(this);
	}
}

const add = (text) => {
	let ftlist = document.getElementById("ft_list");
	let node = document.createElement('div');
	node.innerHTML = text;
	node.onclick = maybe_delete;
	ftlist.insertBefore(node, ftlist.firstChild);
}

const createNew = () => {
	let text = (prompt("New todo").trim());
	if (text !== "") {
		add(text);
		requestPHP('insert.php', {'addtodo': text.replace(';', '\;')});
	}
}

const loadData = () => {
	let xhttp = new XMLHttpRequest();
	xhttp.open("POST", "select.php", true);
	xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			for (todo of JSON.parse(this.response)) {
				add(todo);
			}
		}
	};
	xhttp.send("");
}
<?php
include_once("../ex00/Color.class.php");

class Vertex {
	private $_x;
	private $_y;
	private $_z;
	private $_w;
	private $_color;
	static $verbose = false;

	public static function doc() {
		return (file_get_contents('Vertex.doc.txt'));
	}

	public function __toString() {
		$output = 'class Vertex(_x:'.$this->_x.', y:'.$this->_y. ', z:'.$this->_z.', w:'.$this->_w.')';
		if (self::$verbose === true)
			$output .= $_color.__toString();
		return($output);
	}

	public function __construct(array $vertex) {
		if (array_key_exists('x', $vertex) && array_key_exists('y', $vertex) && array_key_exists('z', $vertex)) {
			$this->_x = $vertex['x'];
			$this->_y = $vertex['y'];
			$this->_z = $vertex['z'];
			$this->_w = (array_key_exists('w', $vertex) ? $vertex['w'] : 1.0);
			$this->$_color = new Color($vertex['color']);						// will result in white if empty

		}
		if (self::$verbose)
			return (__toString());
	}

	public function __destruct() {
		if (self::$verbose)
			return (__toString());
	}

	public function x() {
		return $_x;
	}
	public function y() {
		return $_y;
	}
	public function z() {
		return $_z;
	}
	public function w() {
		return $_x;
	}

}

?>
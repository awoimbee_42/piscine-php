<?php
include_once("../ex01/Vertex.class.php");

class Vector {
	private $_x;
	private $_y;
	private $_z;
	private $_w;
	static $verbose = False;

	public function getX() {
		return $_x;
	}
	public function getY() {
		return $_y;
	}
	public function getZ() {
		return $_z;
	}
	public function getW() {
		return $_w;
	}

	public static function doc() {
		return (file_get_contents('Vector.doc.txt'));
	}

	function __toString() {
		return ("class Vector: x=$this->_x, y=$this->_y, z=$this->_z, w=$this->_w"); // number_format($this->_x, 2, ".", "")
	}

	public function __construct(array $vertices) {
		if (get_class($vertices['org']) == 'Vertex') {
			if (!get_class($vertices['dest']) == 'Vertex')
				$vertices['dest'] = new Vertex(array('x' => 0, 'y' => 0, 'z' =>0));
			$this->_x = $vertices['dest']->x - $vertices['org']->x;
			$this->_y = $vertices['dest']->y - $vertices['org']->y;
			$this->_z = $vertices['dest']->z - $vertices['org']->z;
		}
		// error (bad arg)
		if (self::$verbose)
			return (__toString());
	}

	public function __destruct() {
		if (self::$verbose)
			return (__toString());
	}

	// private static function mult(Vector $v1, Vector $v2) {}
	public function magnitude() {return (sqrt($this->_x*$this->_x + $this->_y*$this->_y + $this->_z*$this->_z));}
	public function normalize() {
		$new = clone $this;
		$mag = $this->magnitude();
		$new->_x = $this->_x / $mag;
		$new->_y = $this->_y / $mag;
		$new->_z = $this->_z / $mag;
		return ($new)
	}
	public function add(Vector $rhs) {
		$x = $this->_x + $rhs->getX();
		$y = $this->_y + $rhs->getY();
		$z = $this->_z + $rhs->getZ();
        return new Vector(['dest' => new Vertex(compact('x', 'y', 'z'))]);
	}
	public function sub(Vector $rhs) {
		$x = $this->_x - $rhs->getX();
		$y = $this->_y - $rhs->getY();
		$z = $this->_z - $rhs->getZ();
		return new Vector(['dest' => new Vertex(compact('x', 'y', 'z'))]);
	}
	public function opposite() {
		$x = -$this->_x;
		$y = -$this->_y;
		$z = -$this->_z;
		return new Vector(['dest' => new Vertex(compact('x', 'y', 'z'))]);
	}
	public function scalarProduct($k) {
		$x = $this->_x * $k;
		$y = $this->_y * $k;
		$z = $this->_z * $k;
		return new Vector(['dest' => new Vertex(compact('x', 'y', 'z'))]);
	}
	public function dotProduct(Vector $rhs) {return ($this->_x*$rhs->getX() + $this->_y*$rhs->getY() + $this->_z*$rhs->getZ());}
	public function cos(Vector $v2) {return (dotProduct($v2) / ($v2.magnitude() * $this.magnitude()));}
	public function crossProduct(Vector $rhs) {                                 //DONT KNOW IF THIS IS GOOD WITH THEIR RIGHT HAND CHINENIGANS, I DONT EVEN KNOW WHICH AXIS IS IN WHICH DIRECTION !
		$x = $this->_y * $rhs->getZ() - $rhs->getY() * $this->_z;
		$y = $this->_z * $rhs->getX() - $rhs->getZ() * $this->_x;
		$z = $this->_x * $rhs->getY() - $rhs->getX() * $this->_y;
		return new Vector(['dest' => new Vertex(compact('x', 'y', 'z'))]);
	}

}


?>
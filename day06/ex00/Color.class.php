<?php
	class Color {
		public $red = 0;
		public $green = 0;
		public $blue = 0;
		static $verbose = false;

		public static function doc() {
			return (file_get_contents('Color.doc.txt'));
		}

		public function __construct(array $colors) {
			if (array_key_exists('rgb', $colors)) {
				$rgb = intval($colors['rgb']);
				$this->$red   = ($rgb & 0xFF0000) >> 16;
				$this->$green = ($rgb & 0x00FF00) >> 8;
				$this->$blue  = ($rgb & 0xFF);
			}
			else if (array_key_exists('red', $colors) || array_key_exists('green', $colors) || array_key_exists('blue', $colors)) {
				$this->$red   = intval($colors['red']);
				$this->$green = intval($colors['green']);
				$this->$blue  = intval($colors['blue']);
			}
			else {
				$this->red = 1;    // ARE THE COLORS IN 0-1 (float) OR IN 0-255 (char) FORMAT ??
				$this->green = 1;
				$this->blue = 1;
			}
			if (self::$verbose)
				return(__toString());
		}
		public function __toString() {
			return('class Color(red: '.sprintf("%3s", $this->red).', green:'.sprintf("%3s", $this->green) . ', blue:'. sprintf("%3s", $this->blue) .' )');
		}


		public function __destruct() {
			if (self::$verbose)
				return(__toString());
			return(NULL);
		}

		public function add(Color $c1, Color $c2) {
			$new_color = new Color(
				array(
					'red' => $c1->red + $c2->red,
					'green' => $c1->green + $c2->green,
					'blue' => $c1->blue + $c2->blue
				));
			return ($new_color);
		}

		public function sub(Color $c1, Color $c2) {
			$new_color = new Color(
				array(
					'red' => $c1->red - $c2->red,
					'green' => $c1->green - $c2->green,
					'blue' => $c1->blue - $c2->blue
				));
			return ($new_color);
		}

		public function mult(Color $c1, Color $c2) {
			$new_color = new Color(
				array(
					'red'	=> $c1->red   * $c2->red,
					'green'	=> $c1->green * $c2->green,
					'blue'	=> $c1->blue  * $c2->blue
				));
			return ($new_color);
		}

	}

?>